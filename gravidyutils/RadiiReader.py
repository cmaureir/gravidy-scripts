import os
import sys
import numpy as np

from utils import *

# IMPORTANT
# Check the common.hpp file to see which distribution of Lagrange Radii you
# are using. By default it should be:
# const float LAGRANGE_RADII[] = {0.01, 0.05, 0.1, 0.2, 0.5, 0.75};

lr = np.array([0.01, 0.05, 0.1, 0.2, 0.5, 0.75])
lr_len =  len(lr) + 2 # Adding '01' and 'Time' columns

class RadiiReader:
    def __init__(self, radiifile):
        self.lr_data = None

        self.time = None
        self.radii = None
        self.radii_percentage = lr

        # Checking if file exist
        if not os.path.exists(radiifile):
            glog_error("Radii file not found | "+radiifile)
            sys.exit(1)
        else:
            glog_done("Reading Radii file | "+radiifile)

        # Reading the whole file
        self.lr_data = np.genfromtxt(radiifile)

        # Each attribute will be an array
        # 01 Time lr0 lr1 lr2 ...
        self.time = self.lr_data[:,1]
        self.radii = np.array([self.lr_data[:,i] for i in range(2,lr_len)])

        glog_done("Finishing data processing")

#if __name__ == "__main__":
#    lr = RadiiReader('../output/plummer1k.out.radii')
#
#    #print(lr.lr_data)
#    print(lr.radii[4])
#    print(lr.radii_percentage[0])
