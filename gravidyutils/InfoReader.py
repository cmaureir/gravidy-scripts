import os
import sys
import numpy as np

from utils import *

class InfoReader:
    def __init__(self, logfile):
        self.info_data = {'NumberParticles': None, # int
                          'Softening': None,       # float
                          'EtaTimestep': None,     # float
                          'IntegrationTime': None, # int
                          'PrintScreen': None,     # bool
                          'InputFilename': None,   # string
                          'OutputFilename': None,  # string
                          'SnapshotNumber': None}  # int

        # Checking if file exist
        if not os.path.exists(logfile):
            glog_error("Info file not found | "+logfile)
            sys.exit(1)
        else:
            glog_done("Reading Infofile | "+logfile)

        # Reading first lines (header)
        with open(logfile) as f:
            try:
                for line in f.readlines():
                    ss = line.strip().replace(" ", "").split(":")

                    if ss[0][0] == "#" and len(ss) == 2:

                        key, value = ss
                        key = key.replace("#", "")

                        self.info_data[key] = value
            except:
                glog_error("Couldn't read the information")

        glog_done("Finishing data processing")

#if __name__ == "__main__":
#    lr = InfoReader('../output/plummer1k.out.info')
#
#    print(lr.info_data)
