import os
import sys
import numpy as np

from utils import *


class LogReader:
    def __init__(self, logfile):
        self.log_data = None

        self.time = None
        self.iter = None
        self.nsteps = None
        self.energy = None
        self.rel_energy = None
        self.cum_energy = None
        self.clock_time = None
        self.elapsed_time = None
        self.gflops = None

        # Checking if file exist
        if not os.path.exists(logfile):
            glog_error("Log file not found | "+logfile)
            sys.exit(1)
        else:
            glog_done("Reading Logfile | "+logfile)

        # Reading the whole file
        self.log_data = np.genfromtxt(logfile)

        # Each attribute will be an array
        # 00 Time Ite Nsteps Energy RelEnergy CumEnergy ElapsedTime GFLOPS

        self.time = self.log_data[:,1]
        self.iter = self.log_data[:,2]
        self.nsteps = self.log_data[:,3]
        self.energy = self.log_data[:,4]
        self.rel_energy = self.log_data[:,5]
        self.cum_energy = self.log_data[:,6]
        self.clock_time = self.log_data[:,7]
        self.elapsed_time = self.clock_time
        self.gflops = self.log_data[:,8]

        glog_done("Finishing data processing")

#if __name__ == "__main__":
#    lr = LogReader('../output/plummer1k.out.log')
#
#    print(lr.log_data)
#    print(lr.energy)
#    print(lr.rel_energy)
#    print(lr.clock_time)
