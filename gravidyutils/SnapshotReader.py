import os
import sys
import numpy as np

from utils import *

class SnapshotReader:
    def __init__(self, snapshot):
        self.snapshot_data = None

        self.time = None
        self.id = None
        self.mass = None
        self.rx = self.ry = self.rz = None
        self.vx = self.vy = self.vz = None
        self.r = [None, None, None]
        self.v = [None, None, None]

        # Checking if file exist
        # TODO: Adding directory support  os.path.isdir
        if not os.path.exists(snapshot):
            glog_error("Snapshot not found | "+snapshot)
            sys.exit(1)
        else:
            glog_done("Reading Snapshot | "+snapshot)

        # Reading first line for Time
        with open(snapshot) as f:
            try:
                line = f.readline()
                ss = line.split(":")
                assert(len(ss) == 2)
                self.time = float(ss[-1])
            except:
                glog_error("Couldn't read Time from snapshot (first line)")

        # Reading the whole file
        self.snapshot_data = np.genfromtxt(snapshot)

        # Each attribute will be an array
        # id mass rx ry rz vx vy vz

        self.id   = self.snapshot_data[:,0]
        self.mass = self.snapshot_data[:,1]

        self.rx = self.snapshot_data[:,2]
        self.ry = self.snapshot_data[:,3]
        self.rz = self.snapshot_data[:,4]

        self.vx = self.snapshot_data[:,5]
        self.vy = self.snapshot_data[:,6]
        self.vz = self.snapshot_data[:,7]

        # Joining positions and velocities to access all coordinates in one array
        # [rx0, ry0, rz0], [rx1, ry1, rz1], [rx2, ry2, rz2], ...
        # [vx0, vy0, vz0], [vx1, vy1, vz1], [vx2, vy2, vz2], ...
        self.r = np.dstack((self.rx, self.ry, self.rz))[0].ravel()
        self.v = np.dstack((self.vx, self.vy, self.vz))[0].ravel()
        glog_done("Finishing data processing")

#if __name__ == "__main__":
#    sr = SnapshotReader('../output/plummer1k.out.snapshot_0000')
#    print(sr.rx[:3], sr.ry[:3], sr.rz[:3])
#    print(sr.r[:3])
#
#    print(sr.vx[:3], sr.vy[:3], sr.vz[:3])
#    print(sr.v[:3])
