import sys

import time
import datetime

# Terminal Colors
color_black   = '\033[0;30m'
color_red     = '\033[0;31m'
color_green   = '\033[0;32m'
color_yellow  = '\033[0;33m'
color_blue    = '\033[0;34m'
color_purple  = '\033[0;35m'
color_cyan    = '\033[0;36m'
color_white   = '\033[0;37m'
color_disable = '\033[m'

# Generate a Timestamp for the logs
def get_timestamp():
    # String
    s = ""
    s += color_blue
    s += '[%Y-%m-%d %H:%M:%S]'
    s += color_disable

    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime(s)

# Logging
def glog(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def glog_error(msg):
    glog(get_timestamp(), color_red+'[ERROR]'+color_disable, msg)

def glog_done(msg):
    glog(get_timestamp(), color_green+'[DONE]'+color_disable, msg)
