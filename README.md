# GraviDy Scripts

Set of scripts and utilities to handle
GraviDy snapshots and files.

 * `InfoReader`, to parse and read the `.info` file.
 * `LogReader`, to parse and read the `.log` file.
 * `RadiiReader`, to parse and read the `.radii` file.
 * `SnapshotReader`, to parse and read the `snapshot` files
